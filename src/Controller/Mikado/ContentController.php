<?php
declare(strict_types=1);

namespace App\Controller\Mikado;
use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;

/**
 * Content Controller
 *
 * @property \App\Model\Table\ContentTable $Content
 * @method \App\Model\Entity\Content[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContentController extends AppController
{
    public function beforeFilter($event) {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('mikado');
        $this->Locales = TableRegistry::getTableLocator()->get('Locales');
        I18n::setLocale('de_DE');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Pages', 'Users'],
        ];
        $content = $this->paginate($this->Content);

        $this->set(compact('content'));
    }

    /**
     * View method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $content = $this->Content->get($id, [
            'contain' => ['Pages', 'Users']
        ]);
        echo '<pre>'; var_dump($content->toArray()); echo '</pre>'; die();

        $this->set(compact('content'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $content = $this->Content->newEmptyEntity();
        if ($this->request->is('post')) {
            $content = $this->Content->patchEntity($content, $this->request->getData());
            if ($this->Content->save($content)) {
                $this->Flash->success(__('The content has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The content could not be saved. Please, try again.'));
        }
        $pages = $this->Content->Pages->find('list', ['limit' => 200])->all();
        $users = $this->Content->Users->find('list', ['limit' => 200])->all();
        $locales = $this->Locales->find()->all();
        $this->set(compact('content', 'pages', 'users', 'locales'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $content = $this->Content->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $content = $this->Content->patchEntity($content, $this->request->getData());
            if ($this->Content->save($content)) {
                $this->Flash->success(__('The content has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The content could not be saved. Please, try again.'));
        }
        $pages = $this->Content->Pages->find('list', ['limit' => 200])->all();
        $users = $this->Content->Users->find('list', ['limit' => 200])->all();
        $locales = $this->Locales->find()->all();
        $this->set(compact('content', 'pages', 'users', 'locales'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Content id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $content = $this->Content->get($id);
        if ($this->Content->delete($content)) {
            $this->Flash->success(__('The content has been deleted.'));
        } else {
            $this->Flash->error(__('The content could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function groupedBySlot() {
        $content = $this->Content->find('groupedBySlot')->where(['page_id' => 3]);
        \Cake\Error\Debugger::dump($content->toArray());
        die();
    }

}
