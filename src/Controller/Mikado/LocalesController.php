<?php
declare(strict_types=1);

namespace App\Controller\Mikado;
use App\Controller\AppController;

/**
 * Locales Controller
 *
 * @property \App\Model\Table\LocalesTable $Locales
 * @method \App\Model\Entity\Locale[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LocalesController extends AppController
{

    public function beforeFilter($event) {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('mikado');
    }



    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $locales = $this->paginate($this->Locales);

        $this->set(compact('locales'));
    }

    /**
     * View method
     *
     * @param string|null $id Locale id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $locale = $this->Locales->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('locale'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $locale = $this->Locales->newEmptyEntity();
        if ($this->request->is('post')) {
            $locale = $this->Locales->patchEntity($locale, $this->request->getData());
            if ($this->Locales->save($locale)) {
                $this->Flash->success(__('The locale has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The locale could not be saved. Please, try again.'));
        }
        $this->set(compact('locale'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Locale id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $locale = $this->Locales->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $locale = $this->Locales->patchEntity($locale, $this->request->getData());
            if ($this->Locales->save($locale)) {
                $this->Flash->success(__('The locale has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The locale could not be saved. Please, try again.'));
        }
        $this->set(compact('locale'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Locale id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $locale = $this->Locales->get($id);
        if ($this->Locales->delete($locale)) {
            $this->Flash->success(__('The locale has been deleted.'));
        } else {
            $this->Flash->error(__('The locale could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
