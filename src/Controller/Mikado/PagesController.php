<?php
declare(strict_types=1);

namespace App\Controller\Mikado;
use App\Controller\AppController;

/**
 * Pages Controller
 *
 * @property \App\Model\Table\PagesTable $Pages
 * @method \App\Model\Entity\Page[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PagesController extends AppController
{

    public function beforeFilter($event) {
        parent::beforeFilter($event);
        $this->viewBuilder()->setLayout('mikado');
    }


    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentPages', 'Users'],
        ];
        $pages = $this->paginate($this->Pages);

        $this->set(compact('pages'));
    }

    /**
     * View method
     *
     * @param string|null $id Page id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $page = $this->Pages->get($id, [
            'contain' => ['ParentPages', 'Users', 'ChildPages'],
        ]);

        $this->set(compact('page'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $page = $this->Pages->newEmptyEntity();
        if ($this->request->is('post')) {
            $page = $this->Pages->patchEntity($page, $this->request->getData());
            if ($this->Pages->save($page)) {
                $this->Flash->success(__('The page has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The page could not be saved. Please, try again.'));
        }
        $parentPages = $this->Pages->ParentPages->find('list', ['limit' => 200])->all();
        $users = $this->Pages->Users->find('list', ['limit' => 200])->all();
        $this->set(compact('page', 'parentPages', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Page id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $page = $this->Pages->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $page = $this->Pages->patchEntity($page, $this->request->getData());
            if ($this->Pages->save($page)) {
                $this->Flash->success(__('The page has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The page could not be saved. Please, try again.'));
        }
        $parentPages = $this->Pages->ParentPages->find('treeList', ['spacer' => '__', 'limit' => 200])->all();
        $users = $this->Pages->Users->find('list', ['limit' => 200])->all();
        $this->set(compact('page', 'parentPages', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Page id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $page = $this->Pages->get($id);
        if ($this->Pages->delete($page)) {
            $this->Flash->success(__('The page has been deleted.'));
        } else {
            $this->Flash->error(__('The page could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }


    public function layoutmode($id = null) {
        $page = $this->Pages->get($id, [
            'contain' => [
                'ParentPages',
                'Users',
                'ChildPages',
            ]
        ]);
        $pages = $this->Pages->find('children', ['for' => 2])
                             ->find('threaded')
                         ;

        $content = $this->Pages->Content->find('groupedBySlot')->where([
            'page_id' => $page->id,
            'is_visible' => true,
            'is_trashed' => false
        ]);
        $this->set(compact('page', 'pages', 'content'));
    }
}

