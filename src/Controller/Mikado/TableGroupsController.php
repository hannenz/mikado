<?php
declare(strict_types=1);

namespace App\Controller\Mikado;
use App\Controller\AppController;

/**
 * TableGroups Controller
 *
 * @property \App\Model\Table\TableGroupsTable $TableGroups
 * @method \App\Model\Entity\TableGroup[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TableGroupsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $tableGroups = $this->paginate($this->TableGroups);

        $this->set(compact('tableGroups'));
    }

    /**
     * View method
     *
     * @param string|null $id Table Group id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $tableGroup = $this->TableGroups->get($id, [
            'contain' => ['Tables'],
        ]);

        $this->set(compact('tableGroup'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $tableGroup = $this->TableGroups->newEmptyEntity();
        if ($this->request->is('post')) {
            $tableGroup = $this->TableGroups->patchEntity($tableGroup, $this->request->getData());
            if ($this->TableGroups->save($tableGroup)) {
                $this->Flash->success(__('The table group has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The table group could not be saved. Please, try again.'));
        }
        $this->set(compact('tableGroup'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Table Group id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $tableGroup = $this->TableGroups->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $tableGroup = $this->TableGroups->patchEntity($tableGroup, $this->request->getData());
            if ($this->TableGroups->save($tableGroup)) {
                $this->Flash->success(__('The table group has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The table group could not be saved. Please, try again.'));
        }
        $this->set(compact('tableGroup'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Table Group id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $tableGroup = $this->TableGroups->get($id);
        if ($this->TableGroups->delete($tableGroup)) {
            $this->Flash->success(__('The table group has been deleted.'));
        } else {
            $this->Flash->error(__('The table group could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
