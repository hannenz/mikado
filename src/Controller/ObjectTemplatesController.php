<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * ObjectTemplates Controller
 *
 * @property \App\Model\Table\ObjectTemplatesTable $ObjectTemplates
 * @method \App\Model\Entity\ObjectTemplate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ObjectTemplatesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['ParentObjectTemplates', 'Users'],
        ];
        $objectTemplates = $this->paginate($this->ObjectTemplates);

        $this->set(compact('objectTemplates'));
    }

    /**
     * View method
     *
     * @param string|null $id Object Template id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $objectTemplate = $this->ObjectTemplates->get($id, [
            'contain' => ['ParentObjectTemplates', 'Users', 'ChildObjectTemplates'],
        ]);

        $this->set(compact('objectTemplate'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $objectTemplate = $this->ObjectTemplates->newEmptyEntity();
        if ($this->request->is('post')) {
            $objectTemplate = $this->ObjectTemplates->patchEntity($objectTemplate, $this->request->getData());
            if ($this->ObjectTemplates->save($objectTemplate)) {
                $this->Flash->success(__('The object template has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The object template could not be saved. Please, try again.'));
        }
        $parentObjectTemplates = $this->ObjectTemplates->ParentObjectTemplates->find('list', ['limit' => 200])->all();
        $users = $this->ObjectTemplates->Users->find('list', ['limit' => 200])->all();
        $this->set(compact('objectTemplate', 'parentObjectTemplates', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Object Template id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $objectTemplate = $this->ObjectTemplates->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $objectTemplate = $this->ObjectTemplates->patchEntity($objectTemplate, $this->request->getData());
            if ($this->ObjectTemplates->save($objectTemplate)) {
                $this->Flash->success(__('The object template has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The object template could not be saved. Please, try again.'));
        }
        $parentObjectTemplates = $this->ObjectTemplates->ParentObjectTemplates->find('list', ['limit' => 200])->all();
        $users = $this->ObjectTemplates->Users->find('list', ['limit' => 200])->all();
        $this->set(compact('objectTemplate', 'parentObjectTemplates', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Object Template id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $objectTemplate = $this->ObjectTemplates->get($id);
        if ($this->ObjectTemplates->delete($objectTemplate)) {
            $this->Flash->success(__('The object template has been deleted.'));
        } else {
            $this->Flash->error(__('The object template could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
