<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * PageTemplates Controller
 *
 * @property \App\Model\Table\PageTemplatesTable $PageTemplates
 * @method \App\Model\Entity\PageTemplate[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PageTemplatesController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $pageTemplates = $this->paginate($this->PageTemplates);

        $this->set(compact('pageTemplates'));
    }

    /**
     * View method
     *
     * @param string|null $id Page Template id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $pageTemplate = $this->PageTemplates->get($id, [
            'contain' => [],
        ]);

        $this->set(compact('pageTemplate'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $pageTemplate = $this->PageTemplates->newEmptyEntity();
        if ($this->request->is('post')) {
            $pageTemplate = $this->PageTemplates->patchEntity($pageTemplate, $this->request->getData());
            if ($this->PageTemplates->save($pageTemplate)) {
                $this->Flash->success(__('The page template has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The page template could not be saved. Please, try again.'));
        }
        $this->set(compact('pageTemplate'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Page Template id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $pageTemplate = $this->PageTemplates->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $pageTemplate = $this->PageTemplates->patchEntity($pageTemplate, $this->request->getData());
            if ($this->PageTemplates->save($pageTemplate)) {
                $this->Flash->success(__('The page template has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The page template could not be saved. Please, try again.'));
        }
        $this->set(compact('pageTemplate'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Page Template id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $pageTemplate = $this->PageTemplates->get($id);
        if ($this->PageTemplates->delete($pageTemplate)) {
            $this->Flash->success(__('The page template has been deleted.'));
        } else {
            $this->Flash->error(__('The page template could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
