<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;

/**
 * Content Entity
 *
 * @property int $id
 * @property int $page_id
 * @property string $slot
 * @property bool $is_visible
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int $user_id
 * @property bool $is_trashed
 * @property string|null $head1
 * @property string|null $head2
 * @property string|null $head3
 * @property string|null $head4
 * @property string|null $head5
 * @property string|null $text1
 * @property string|null $text2
 * @property string|null $text3
 * @property string|null $text4
 * @property string|null $text5
 * @property string|null $html1
 * @property string|null $file1
 * @property int $pos
 *
 * @property \App\Model\Entity\Page $page
 * @property \App\Model\Entity\User $user
 */
class Content extends Entity
{

    use TranslateTrait;



    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'page_id' => true,
        'slot' => true,
        'template' => true,
        'is_visible' => true,
        'created' => true,
        'modified' => true,
        'user_id' => true,
        'is_trashed' => true,
        'head1' => true,
        'head2' => true,
        'head3' => true,
        'head4' => true,
        'head5' => true,
        'text1' => true,
        'text2' => true,
        'text3' => true,
        'text4' => true,
        'text5' => true,
        'html1' => true,
        'file1' => true,
        'pos' => true,
        'page' => true,
        'user' => true,
        '_translations' => true
    ];
}
