<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Field Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $alias
 * @property string $type
 * @property string|null $options
 * @property string|null $default
 * @property int $table_id
 *
 * @property \App\Model\Entity\Table $table
 */
class Field extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'name' => true,
        'alias' => true,
        'type' => true,
        'options' => true,
        'default' => true,
        'table_id' => true,
        'table' => true,
    ];
}
