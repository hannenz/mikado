<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ObjectTemplate Entity
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $file
 * @property int|null $parent_id
 * @property int|null $lft
 * @property int|null $rght
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int $user_id
 *
 * @property \App\Model\Entity\ParentObjectTemplate $parent_object_template
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\ChildObjectTemplate[] $child_object_templates
 */
class ObjectTemplate extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'name' => true,
        'file' => true,
        'parent_id' => true,
        'lft' => true,
        'rght' => true,
        'created' => true,
        'modified' => true,
        'user_id' => true,
        'parent_object_template' => true,
        'user' => true,
        'child_object_templates' => true,
    ];
}
