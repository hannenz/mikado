<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Page Entity
 *
 * @property int $id
 * @property string|null $title
 * @property int|null $parent_id
 * @property int|null $lft
 * @property int|null $rght
 * @property string|null $page_type
 * @property string|null $meta_description
 * @property string|null $meta_keywords
 * @property string|null $slug
 * @property bool $is_visible
 * @property bool $is_link
 * @property string|null $link_target
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int $user_id
 *
 * @property \App\Model\Entity\Page $parent_page
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\Page[] $child_pages
 */
class Page extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'title' => true,
        'parent_id' => true,
        'lft' => true,
        'rght' => true,
        'page_type' => true,
        'meta_description' => true,
        'meta_keywords' => true,
        'slug' => true,
        'is_visible' => true,
        'is_link' => true,
        'link_target' => true,
        'created' => true,
        'modified' => true,
        'user_id' => true,
        'parent_page' => true,
        'user' => true,
        'child_pages' => true,
        'layout' => true
    ];
}
