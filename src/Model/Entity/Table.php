<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Table Entity
 *
 * @property int $id
 * @property string $name
 * @property string|null $alias
 * @property int $pos
 * @property int $table_group_id
 * @property string|null $include
 * @property string $type
 * @property bool $is_visible
 * @property string|null $options
 *
 * @property \App\Model\Entity\TableGroup $table_group
 * @property \App\Model\Entity\Field[] $fields
 */
class Table extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'name' => true,
        'alias' => true,
        'pos' => true,
        'table_group_id' => true,
        'include' => true,
        'type' => true,
        'is_visible' => true,
        'options' => true,
        'table_group' => true,
        'fields' => true,
    ];
}
