<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * TableGroup Entity
 *
 * @property int $id
 * @property string $name
 * @property int $pos
 * @property bool $is_visible
 * @property string|null $icon
 * @property string|null $options
 *
 * @property \App\Model\Entity\Table[] $tables
 */
class TableGroup extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array<string, bool>
     */
    protected $_accessible = [
        'name' => true,
        'pos' => true,
        'is_visible' => true,
        'icon' => true,
        'options' => true,
        'tables' => true,
    ];
}
