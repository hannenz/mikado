<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Content Model
 *
 * @property \App\Model\Table\PagesTable&\Cake\ORM\Association\BelongsTo $Pages
 * @property \App\Model\Table\UsersTable&\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Content newEmptyEntity()
 * @method \App\Model\Entity\Content newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Content[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Content get($primaryKey, $options = [])
 * @method \App\Model\Entity\Content findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Content patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Content[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Content|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Content saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Content[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Content[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Content[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Content[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ContentTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('content');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        $this->addBehavior('Translate', ['fields' => [
            'head1', 'head2', 'head3', 'head4', 'head5',
            'text1', 'text2', 'text3', 'text4', 'text5',
            'html1',
            'file1'
            ]
        ]);

        $this->belongsTo('Pages', [
            'foreignKey' => 'page_id',
            'joinType' => 'INNER',
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('page_id')
            ->requirePresence('page_id', 'create')
            ->notEmptyString('page_id');

        $validator
            ->scalar('template')
            ->maxLength('template', 255)
            ->requirePresence('template', 'create')
            ->notEmptyString('template');

        $validator
            ->scalar('slot')
            ->maxLength('slot', 255)
            ->requirePresence('slot', 'create')
            ->notEmptyString('slot');

        $validator
            ->boolean('is_visible')
            ->requirePresence('is_visible', 'create')
            ->notEmptyString('is_visible');

        $validator
            ->integer('user_id')
            ->requirePresence('user_id', 'create')
            ->notEmptyString('user_id');

        $validator
            ->boolean('is_trashed')
            ->requirePresence('is_trashed', 'create')
            ->notEmptyString('is_trashed');

        $validator
            ->scalar('head1')
            ->maxLength('head1', 255)
            ->allowEmptyString('head1');

        $validator
            ->scalar('head2')
            ->maxLength('head2', 255)
            ->allowEmptyString('head2');

        $validator
            ->scalar('head3')
            ->maxLength('head3', 255)
            ->allowEmptyString('head3');

        $validator
            ->scalar('head4')
            ->maxLength('head4', 255)
            ->allowEmptyString('head4');

        $validator
            ->scalar('head5')
            ->maxLength('head5', 255)
            ->allowEmptyString('head5');

        $validator
            ->scalar('text1')
            ->allowEmptyString('text1');

        $validator
            ->scalar('text2')
            ->allowEmptyString('text2');

        $validator
            ->scalar('text3')
            ->allowEmptyString('text3');

        $validator
            ->scalar('text4')
            ->allowEmptyString('text4');

        $validator
            ->scalar('text5')
            ->allowEmptyString('text5');

        $validator
            ->scalar('html1')
            ->allowEmptyString('html1');

        $validator
            ->scalar('file1')
            ->maxLength('file1', 255)
            ->allowEmptyFile('file1');

        $validator
            ->integer('pos')
            ->requirePresence('pos', 'create')
            ->notEmptyString('pos');

        return $validator;
    }


    public function findGroupedBySlot(Query $query, array $options = []) {
        // Group by slots (a.k.a. "Blocks")
        $mapper = function($item, $key, $mapReduce) {
            $mapReduce->emitIntermediate($item, $item->slot);
        };

        $reducer = function($results, $slot, $mapReduce) {
            $mapReduce->emit($results, $slot);
        };
        $query->mapReduce($mapper, $reducer);
        return $query;
    }


    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules): RulesChecker
    {
        $rules->add($rules->existsIn('page_id', 'Pages'), ['errorField' => 'page_id']);
        $rules->add($rules->existsIn('user_id', 'Users'), ['errorField' => 'user_id']);

        return $rules;
    }
}
