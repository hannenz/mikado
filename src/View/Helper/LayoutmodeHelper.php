<?php
namespace App\View\Helper;

use Cake\View\Helper;

class LayoutmodeHelper extends Helper {

    public $helpers = ['Html'];

    public function element($element, $varname) {
        return sprintf('<div data-mikado-element-id="%u">%s</div>', $element->id, $object[$varname]);
    }

    public function getContent($key) {
        return $this->data[$key];
    }
}
