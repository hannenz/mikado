<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Field $field
 * @var \Cake\Collection\CollectionInterface|string[] $tables
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Fields'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="fields form content">
            <?= $this->Form->create($field) ?>
            <fieldset>
                <legend><?= __('Add Field') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('alias');
                    echo $this->Form->control('type');
                    echo $this->Form->control('options');
                    echo $this->Form->control('default');
                    echo $this->Form->control('table_id', ['options' => $tables]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
