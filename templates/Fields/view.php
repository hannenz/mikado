<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Field $field
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Field'), ['action' => 'edit', $field->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Field'), ['action' => 'delete', $field->id], ['confirm' => __('Are you sure you want to delete # {0}?', $field->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Fields'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Field'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="fields view content">
            <h3><?= h($field->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($field->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Alias') ?></th>
                    <td><?= h($field->alias) ?></td>
                </tr>
                <tr>
                    <th><?= __('Type') ?></th>
                    <td><?= h($field->type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Table') ?></th>
                    <td><?= $field->has('table') ? $this->Html->link($field->table->name, ['controller' => 'Tables', 'action' => 'view', $field->table->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($field->id) ?></td>
                </tr>
            </table>
            <div class="text">
                <strong><?= __('Options') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($field->options)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Default') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($field->default)); ?>
                </blockquote>
            </div>
        </div>
    </div>
</div>
