<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 * @var string[]|\Cake\Collection\CollectionInterface $pages
 * @var string[]|\Cake\Collection\CollectionInterface $objectTemplates
 * @var string[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $content->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $content->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Content'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="content form content">
            <?= $this->Form->create($content) ?>
            <fieldset>
                <legend><?= __('Edit Content') ?></legend>
                <?php
                    echo $this->Form->control('page_id', ['options' => $pages]);
                    // echo $this->Form->control('object_template_id', ['options' => $objectTemplates]);
                    echo $this->Form->control('template');
                    echo $this->Form->control('slot');
                    echo $this->Form->control('is_visible');
                    echo $this->Form->control('user_id', ['options' => $users]);
                    echo $this->Form->control('is_trashed');
                    echo $this->Form->control('pos');
                ?>
            </fieldset>
            <?php foreach ($locales as $locale): ?>
                <fieldset>
                    <legend>Content <?php echo $locale['name']; ?></legend>
                    <?php
                            echo $this->Form->control('_translations.'.$locale['lc'].'.head1', ['label' => "head1 [{$locale['name']}]"]);
                            echo $this->Form->control('_translations.'.$locale['lc'].'.head2', ['label' => "head2 [{$locale['name']}]"]);
                            echo $this->Form->control('_translations.'.$locale['lc'].'.head3', ['label' => "head3 [{$locale['name']}]"]);
                            echo $this->Form->control('_translations.'.$locale['lc'].'.head4', ['label' => "head4 [{$locale['name']}]"]);
                            echo $this->Form->control('_translations.'.$locale['lc'].'.head5', ['label' => "head5 [{$locale['name']}]"]);
                            echo $this->Form->control('_translations.'.$locale['lc'].'.text1', ['label' => "text1 [{$locale['name']}]"]);
                            echo $this->Form->control('_translations.'.$locale['lc'].'.text2', ['label' => "text2 [{$locale['name']}]"]);
                            echo $this->Form->control('_translations.'.$locale['lc'].'.text3', ['label' => "text3 [{$locale['name']}]"]);
                            echo $this->Form->control('_translations.'.$locale['lc'].'.text4', ['label' => "text4 [{$locale['name']}]"]);
                            echo $this->Form->control('_translations.'.$locale['lc'].'.text5', ['label' => "text5 [{$locale['name']}]"]);
                            echo $this->Form->control('_translations.'.$locale['lc'].'.html1', ['label' => "html1 [{$locale['name']}]"]);
                            echo $this->Form->control('_translations.'.$locale['lc'].'.file1', ['label' => "file1 [{$locale['name']}]"]);
                        ?>
                    </fieldset>
                <?php endforeach ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
