<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content[]|\Cake\Collection\CollectionInterface $content
 */
?>
<div class="content index content">
    <?= $this->Html->link(__('New Content'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Content') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('page_id') ?></th>
                    <th><?= $this->Paginator->sort('template') ?></th>
                    <th><?= $this->Paginator->sort('slot') ?></th>
                    <th><?= $this->Paginator->sort('head1') ?></th>
                    <th><?= $this->Paginator->sort('text1') ?></th>
                    <th><?= $this->Paginator->sort('is_visible') ?></th>
                    <th><?= $this->Paginator->sort('is_trashed') ?></th>
                    <th><?= $this->Paginator->sort('pos') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($content as $content): ?>
                <tr>
                    <td><?= $this->Number->format($content->id) ?></td>
                    <td><?= $content->has('page') ? $this->Html->link($content->page->title, ['controller' => 'Pages', 'action' => 'view', $content->page->id]) : '' ?></td>
                    <td><?= h($content->template) ?></td>
                    <td><?= h($content->slot) ?></td>
                    <td><?= h($content->head1) ?></td>
                    <td><?= h($content->text1) ?></td>
                    <td><?= h($content->is_visible) ?></td>
                    <td><?= h($content->is_trashed) ?></td>
                    <td><?= $this->Number->format($content->pos) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $content->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $content->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $content->id], ['confirm' => __('Are you sure you want to delete # {0}?', $content->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
