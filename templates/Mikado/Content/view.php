<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Content $content
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Content'), ['action' => 'edit', $content->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Content'), ['action' => 'delete', $content->id], ['confirm' => __('Are you sure you want to delete # {0}?', $content->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Content'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Content'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="content view content">
            <h3><?= h($content->id) ?></h3>
            <table>
                <tr>
                    <th><?= __('Page') ?></th>
                    <td><?= $content->has('page') ? $this->Html->link($content->page->title, ['controller' => 'Pages', 'action' => 'view', $content->page->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Object Template') ?></th>
                    <td><?= $content->has('object_template') ? $this->Html->link($content->object_template->name, ['controller' => 'ObjectTemplates', 'action' => 'view', $content->object_template->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Slot') ?></th>
                    <td><?= h($content->slot) ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $content->has('user') ? $this->Html->link($content->user->id, ['controller' => 'Users', 'action' => 'view', $content->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Head1') ?></th>
                    <td><?= h($content->head1) ?></td>
                </tr>
                <tr>
                    <th><?= __('Head2') ?></th>
                    <td><?= h($content->head2) ?></td>
                </tr>
                <tr>
                    <th><?= __('Head3') ?></th>
                    <td><?= h($content->head3) ?></td>
                </tr>
                <tr>
                    <th><?= __('Head4') ?></th>
                    <td><?= h($content->head4) ?></td>
                </tr>
                <tr>
                    <th><?= __('Head5') ?></th>
                    <td><?= h($content->head5) ?></td>
                </tr>
                <tr>
                    <th><?= __('File1') ?></th>
                    <td><?= h($content->file1) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($content->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Pos') ?></th>
                    <td><?= $this->Number->format($content->pos) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($content->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($content->modified) ?></td>
                </tr>
                <tr>
                    <th><?= __('Is Visible') ?></th>
                    <td><?= $content->is_visible ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th><?= __('Is Trashed') ?></th>
                    <td><?= $content->is_trashed ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
            <div class="text">
                <strong><?= __('Text1') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($content->text1)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Text2') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($content->text2)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Text3') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($content->text3)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Text4') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($content->text4)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Text5') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($content->text5)); ?>
                </blockquote>
            </div>
            <div class="text">
                <strong><?= __('Html1') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($content->html1)); ?>
                </blockquote>
            </div>
        </div>
    </div>
</div>
