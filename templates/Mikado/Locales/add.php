<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Locale $locale
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Locales'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="locales form content">
            <?= $this->Form->create($locale) ?>
            <fieldset>
                <legend><?= __('Add Locale') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('lc');
                    echo $this->Form->control('pos');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
