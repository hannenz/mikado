<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Locale[]|\Cake\Collection\CollectionInterface $locales
 */
?>
<div class="locales index content">
    <?= $this->Html->link(__('New Locale'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Locales') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('lc') ?></th>
                    <th><?= $this->Paginator->sort('pos') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($locales as $locale): ?>
                <tr>
                    <td><?= $this->Number->format($locale->id) ?></td>
                    <td><?= h($locale->name) ?></td>
                    <td><?= h($locale->lc) ?></td>
                    <td><?= $this->Number->format($locale->pos) ?></td>
                    <td><?= h($locale->created) ?></td>
                    <td><?= h($locale->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $locale->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $locale->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $locale->id], ['confirm' => __('Are you sure you want to delete # {0}?', $locale->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
