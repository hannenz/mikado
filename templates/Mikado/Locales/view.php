<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Locale $locale
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Locale'), ['action' => 'edit', $locale->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Locale'), ['action' => 'delete', $locale->id], ['confirm' => __('Are you sure you want to delete # {0}?', $locale->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Locales'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Locale'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="locales view content">
            <h3><?= h($locale->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($locale->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lc') ?></th>
                    <td><?= h($locale->lc) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($locale->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Pos') ?></th>
                    <td><?= $this->Number->format($locale->pos) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($locale->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($locale->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
