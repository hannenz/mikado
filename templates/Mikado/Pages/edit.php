<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Page $page
 * @var string[]|\Cake\Collection\CollectionInterface $parentPages
 * @var string[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $page->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $page->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Pages'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="pages form content">
            <?= $this->Form->create($page) ?>
            <fieldset>
                <legend><?= __('Edit Page') ?></legend>
                <?php
                    echo $this->Form->control('title');
                    echo $this->Form->control('parent_id', ['options' => $parentPages, 'empty' => false]);
                    echo $this->Form->control('layout');
                    echo $this->Form->control('page_type', ['options' => [
                        'page' => __('Page'),
                        'directory' => __('Directory'),
                        'link' => __('Link')
                    ], 'empty' => false]);
                    echo $this->Form->control('is_home');
                    echo $this->Form->control('meta_description');
                    echo $this->Form->control('meta_keywords');
                    echo $this->Form->control('slug');
                    echo $this->Form->control('is_visible');
                    echo $this->Form->control('is_link');
                    echo $this->Form->control('link_target');
                    echo $this->Form->control('user_id', ['options' => $users]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
