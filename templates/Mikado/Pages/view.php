<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Page $page
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Page'), ['action' => 'edit', $page->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Page'), ['action' => 'delete', $page->id], ['confirm' => __('Are you sure you want to delete # {0}?', $page->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Pages'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Page'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="pages view content">
            <h3><?= h($page->title) ?></h3>
            <table>
                <tr>
                    <th><?= __('Title') ?></th>
                    <td><?= h($page->title) ?></td>
                </tr>
                <tr>
                    <th><?= __('Parent Page') ?></th>
                    <td><?= $page->has('parent_page') ? $this->Html->link($page->parent_page->title, ['controller' => 'Pages', 'action' => 'view', $page->parent_page->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Page Template') ?></th>
                    <td><?= $page->has('page_template') ? $this->Html->link($page->page_template->name, ['controller' => 'PageTemplates', 'action' => 'view', $page->page_template->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Page Type') ?></th>
                    <td><?= h($page->page_type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Meta Keywords') ?></th>
                    <td><?= h($page->meta_keywords) ?></td>
                </tr>
                <tr>
                    <th><?= __('Slug') ?></th>
                    <td><?= h($page->slug) ?></td>
                </tr>
                <tr>
                    <th><?= __('Link Target') ?></th>
                    <td><?= h($page->link_target) ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $page->has('user') ? $this->Html->link($page->user->id, ['controller' => 'Users', 'action' => 'view', $page->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($page->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lft') ?></th>
                    <td><?= $page->lft === null ? '' : $this->Number->format($page->lft) ?></td>
                </tr>
                <tr>
                    <th><?= __('Rght') ?></th>
                    <td><?= $page->rght === null ? '' : $this->Number->format($page->rght) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($page->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($page->modified) ?></td>
                </tr>
                <tr>
                    <th><?= __('Is Visible') ?></th>
                    <td><?= $page->is_visible ? __('Yes') : __('No'); ?></td>
                </tr>
                <tr>
                    <th><?= __('Is Link') ?></th>
                    <td><?= $page->is_link ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
            <div class="text">
                <strong><?= __('Meta Description') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($page->meta_description)); ?>
                </blockquote>
            </div>
            <div class="related">
                <h4><?= __('Related Pages') ?></h4>
                <?php if (!empty($page->child_pages)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Title') ?></th>
                            <th><?= __('Parent Id') ?></th>
                            <th><?= __('Lft') ?></th>
                            <th><?= __('Rght') ?></th>
                            <th><?= __('Template Id') ?></th>
                            <th><?= __('Page Type') ?></th>
                            <th><?= __('Meta Description') ?></th>
                            <th><?= __('Meta Keywords') ?></th>
                            <th><?= __('Slug') ?></th>
                            <th><?= __('Is Visible') ?></th>
                            <th><?= __('Is Link') ?></th>
                            <th><?= __('Link Target') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($page->child_pages as $childPages) : ?>
                        <tr>
                            <td><?= h($childPages->id) ?></td>
                            <td><?= h($childPages->title) ?></td>
                            <td><?= h($childPages->parent_id) ?></td>
                            <td><?= h($childPages->lft) ?></td>
                            <td><?= h($childPages->rght) ?></td>
                            <td><?= h($childPages->template_id) ?></td>
                            <td><?= h($childPages->page_type) ?></td>
                            <td><?= h($childPages->meta_description) ?></td>
                            <td><?= h($childPages->meta_keywords) ?></td>
                            <td><?= h($childPages->slug) ?></td>
                            <td><?= h($childPages->is_visible) ?></td>
                            <td><?= h($childPages->is_link) ?></td>
                            <td><?= h($childPages->link_target) ?></td>
                            <td><?= h($childPages->created) ?></td>
                            <td><?= h($childPages->modified) ?></td>
                            <td><?= h($childPages->user_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Pages', 'action' => 'view', $childPages->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Pages', 'action' => 'edit', $childPages->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Pages', 'action' => 'delete', $childPages->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childPages->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
