<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ObjectTemplate $objectTemplate
 * @var \Cake\Collection\CollectionInterface|string[] $parentObjectTemplates
 * @var \Cake\Collection\CollectionInterface|string[] $users
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Object Templates'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="objectTemplates form content">
            <?= $this->Form->create($objectTemplate) ?>
            <fieldset>
                <legend><?= __('Add Object Template') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('file');
                    echo $this->Form->control('parent_id', ['options' => $parentObjectTemplates, 'empty' => true]);
                    echo $this->Form->control('user_id', ['options' => $users]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
