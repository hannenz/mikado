<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ObjectTemplate $objectTemplate
 * @var string[]|\Cake\Collection\CollectionInterface $parentObjectTemplates
 * @var string[]|\Cake\Collection\CollectionInterface $users
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $objectTemplate->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $objectTemplate->id), 'class' => 'side-nav-item']
            ) ?>
            <?= $this->Html->link(__('List Object Templates'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="objectTemplates form content">
            <?= $this->Form->create($objectTemplate) ?>
            <fieldset>
                <legend><?= __('Edit Object Template') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('file');
                    echo $this->Form->control('parent_id', ['options' => $parentObjectTemplates, 'empty' => true]);
                    echo $this->Form->control('user_id', ['options' => $users]);
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
