<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ObjectTemplate[]|\Cake\Collection\CollectionInterface $objectTemplates
 */
?>
<div class="objectTemplates index content">
    <?= $this->Html->link(__('New Object Template'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Object Templates') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('file') ?></th>
                    <th><?= $this->Paginator->sort('parent_id') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th><?= $this->Paginator->sort('user_id') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($objectTemplates as $objectTemplate): ?>
                <tr>
                    <td><?= $this->Number->format($objectTemplate->id) ?></td>
                    <td><?= h($objectTemplate->name) ?></td>
                    <td><?= h($objectTemplate->file) ?></td>
                    <td><?= $objectTemplate->has('parent_object_template') ? $this->Html->link($objectTemplate->parent_object_template->name, ['controller' => 'ObjectTemplates', 'action' => 'view', $objectTemplate->parent_object_template->id]) : '' ?></td>
                    <td><?= h($objectTemplate->created) ?></td>
                    <td><?= h($objectTemplate->modified) ?></td>
                    <td><?= $objectTemplate->has('user') ? $this->Html->link($objectTemplate->user->id, ['controller' => 'Users', 'action' => 'view', $objectTemplate->user->id]) : '' ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $objectTemplate->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $objectTemplate->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $objectTemplate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $objectTemplate->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
