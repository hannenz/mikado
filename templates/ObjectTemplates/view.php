<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ObjectTemplate $objectTemplate
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Object Template'), ['action' => 'edit', $objectTemplate->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Object Template'), ['action' => 'delete', $objectTemplate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $objectTemplate->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Object Templates'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Object Template'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="objectTemplates view content">
            <h3><?= h($objectTemplate->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($objectTemplate->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('File') ?></th>
                    <td><?= h($objectTemplate->file) ?></td>
                </tr>
                <tr>
                    <th><?= __('Parent Object Template') ?></th>
                    <td><?= $objectTemplate->has('parent_object_template') ? $this->Html->link($objectTemplate->parent_object_template->name, ['controller' => 'ObjectTemplates', 'action' => 'view', $objectTemplate->parent_object_template->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('User') ?></th>
                    <td><?= $objectTemplate->has('user') ? $this->Html->link($objectTemplate->user->id, ['controller' => 'Users', 'action' => 'view', $objectTemplate->user->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($objectTemplate->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Lft') ?></th>
                    <td><?= $objectTemplate->lft === null ? '' : $this->Number->format($objectTemplate->lft) ?></td>
                </tr>
                <tr>
                    <th><?= __('Rght') ?></th>
                    <td><?= $objectTemplate->rght === null ? '' : $this->Number->format($objectTemplate->rght) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($objectTemplate->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($objectTemplate->modified) ?></td>
                </tr>
            </table>
            <div class="related">
                <h4><?= __('Related Object Templates') ?></h4>
                <?php if (!empty($objectTemplate->child_object_templates)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('File') ?></th>
                            <th><?= __('Parent Id') ?></th>
                            <th><?= __('Lft') ?></th>
                            <th><?= __('Rght') ?></th>
                            <th><?= __('Created') ?></th>
                            <th><?= __('Modified') ?></th>
                            <th><?= __('User Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($objectTemplate->child_object_templates as $childObjectTemplates) : ?>
                        <tr>
                            <td><?= h($childObjectTemplates->id) ?></td>
                            <td><?= h($childObjectTemplates->name) ?></td>
                            <td><?= h($childObjectTemplates->file) ?></td>
                            <td><?= h($childObjectTemplates->parent_id) ?></td>
                            <td><?= h($childObjectTemplates->lft) ?></td>
                            <td><?= h($childObjectTemplates->rght) ?></td>
                            <td><?= h($childObjectTemplates->created) ?></td>
                            <td><?= h($childObjectTemplates->modified) ?></td>
                            <td><?= h($childObjectTemplates->user_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'ObjectTemplates', 'action' => 'view', $childObjectTemplates->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'ObjectTemplates', 'action' => 'edit', $childObjectTemplates->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'ObjectTemplates', 'action' => 'delete', $childObjectTemplates->id], ['confirm' => __('Are you sure you want to delete # {0}?', $childObjectTemplates->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
