<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PageTemplate $pageTemplate
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Page Templates'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="pageTemplates form content">
            <?= $this->Form->create($pageTemplate) ?>
            <fieldset>
                <legend><?= __('Add Page Template') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('file');
                    echo $this->Form->control('pos');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
