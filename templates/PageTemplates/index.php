<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PageTemplate[]|\Cake\Collection\CollectionInterface $pageTemplates
 */
?>
<div class="pageTemplates index content">
    <?= $this->Html->link(__('New Page Template'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Page Templates') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('file') ?></th>
                    <th><?= $this->Paginator->sort('pos') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($pageTemplates as $pageTemplate): ?>
                <tr>
                    <td><?= $this->Number->format($pageTemplate->id) ?></td>
                    <td><?= h($pageTemplate->name) ?></td>
                    <td><?= h($pageTemplate->file) ?></td>
                    <td><?= $pageTemplate->pos === null ? '' : $this->Number->format($pageTemplate->pos) ?></td>
                    <td><?= h($pageTemplate->created) ?></td>
                    <td><?= h($pageTemplate->modified) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $pageTemplate->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $pageTemplate->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $pageTemplate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pageTemplate->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
