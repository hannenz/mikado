<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\PageTemplate $pageTemplate
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Page Template'), ['action' => 'edit', $pageTemplate->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Page Template'), ['action' => 'delete', $pageTemplate->id], ['confirm' => __('Are you sure you want to delete # {0}?', $pageTemplate->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Page Templates'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Page Template'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="pageTemplates view content">
            <h3><?= h($pageTemplate->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($pageTemplate->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('File') ?></th>
                    <td><?= h($pageTemplate->file) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($pageTemplate->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Pos') ?></th>
                    <td><?= $pageTemplate->pos === null ? '' : $this->Number->format($pageTemplate->pos) ?></td>
                </tr>
                <tr>
                    <th><?= __('Created') ?></th>
                    <td><?= h($pageTemplate->created) ?></td>
                </tr>
                <tr>
                    <th><?= __('Modified') ?></th>
                    <td><?= h($pageTemplate->modified) ?></td>
                </tr>
            </table>
        </div>
    </div>
</div>
