<?php
// This will assign all view blocks with the elements of the corresponding slots
// and not output anything by itself. The 'content' block is left empty / unused
// in the frontend layout template.
// Use `$this->fetch('slot-name')` to output the slots in the layout template
$this->assign('title', $page->title);
$this->layout = !empty($page->layout) ? $page->layout : 'default';
foreach ($content as $slot => $items) {
    $this->start($slot);
    foreach ($items as $item) {
        echo $this->element($item->template, $item->toArray());
    }
    $this->end();
}
$this->start('main-navigation');
echo '<ul>';
foreach ($pages as $_page) {
   echo '<li>';
   if (empty($_page->children)) {
       echo $this->Html->link($_page->title, ['controller' => 'Pages', 'action' => 'view', $_page->id], ['class' => $_page->id == $page->id ? 'is-current' : '']);
   }
   else {
       printf('<button onclick="%s">%s</button>', "this.toggleAttribute('aria-expanded')", $_page->title);
       echo '<ul>';
       foreach ($_page->children as $_page) {
           echo "<li>";
           if (empty($_page->children)) {
               echo $this->Html->link($_page->title, ['controller' => 'Pages', 'action' => 'view', $_page->id], ['class' => $_page->id == $page->id ? 'is-current' : '']);
           }
           echo "</li>";
       }
       echo "</ul>";
       echo "</li>";
   }
}
$this->end();
?>
