<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TableGroup $tableGroup
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Table Groups'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="tableGroups form content">
            <?= $this->Form->create($tableGroup) ?>
            <fieldset>
                <legend><?= __('Add Table Group') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('pos');
                    echo $this->Form->control('is_visible');
                    echo $this->Form->control('icon');
                    echo $this->Form->control('options');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
