<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TableGroup[]|\Cake\Collection\CollectionInterface $tableGroups
 */
?>
<div class="tableGroups index content">
    <?= $this->Html->link(__('New Table Group'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Table Groups') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('pos') ?></th>
                    <th><?= $this->Paginator->sort('is_visible') ?></th>
                    <th><?= $this->Paginator->sort('icon') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($tableGroups as $tableGroup): ?>
                <tr>
                    <td><?= $this->Number->format($tableGroup->id) ?></td>
                    <td><?= h($tableGroup->name) ?></td>
                    <td><?= $this->Number->format($tableGroup->pos) ?></td>
                    <td><?= h($tableGroup->is_visible) ?></td>
                    <td><?= h($tableGroup->icon) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $tableGroup->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $tableGroup->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $tableGroup->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tableGroup->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
