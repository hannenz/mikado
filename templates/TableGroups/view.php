<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TableGroup $tableGroup
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Table Group'), ['action' => 'edit', $tableGroup->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Table Group'), ['action' => 'delete', $tableGroup->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tableGroup->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Table Groups'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Table Group'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="tableGroups view content">
            <h3><?= h($tableGroup->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($tableGroup->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Icon') ?></th>
                    <td><?= h($tableGroup->icon) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($tableGroup->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Pos') ?></th>
                    <td><?= $this->Number->format($tableGroup->pos) ?></td>
                </tr>
                <tr>
                    <th><?= __('Is Visible') ?></th>
                    <td><?= $tableGroup->is_visible ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
            <div class="text">
                <strong><?= __('Options') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($tableGroup->options)); ?>
                </blockquote>
            </div>
            <div class="related">
                <h4><?= __('Related Tables') ?></h4>
                <?php if (!empty($tableGroup->tables)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Alias') ?></th>
                            <th><?= __('Pos') ?></th>
                            <th><?= __('Table Group Id') ?></th>
                            <th><?= __('Include') ?></th>
                            <th><?= __('Type') ?></th>
                            <th><?= __('Is Visible') ?></th>
                            <th><?= __('Options') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($tableGroup->tables as $tables) : ?>
                        <tr>
                            <td><?= h($tables->id) ?></td>
                            <td><?= h($tables->name) ?></td>
                            <td><?= h($tables->alias) ?></td>
                            <td><?= h($tables->pos) ?></td>
                            <td><?= h($tables->table_group_id) ?></td>
                            <td><?= h($tables->include) ?></td>
                            <td><?= h($tables->type) ?></td>
                            <td><?= h($tables->is_visible) ?></td>
                            <td><?= h($tables->options) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Tables', 'action' => 'view', $tables->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Tables', 'action' => 'edit', $tables->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Tables', 'action' => 'delete', $tables->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tables->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
