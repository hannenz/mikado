<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Table $table
 * @var \Cake\Collection\CollectionInterface|string[] $tableGroups
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Tables'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="tables form content">
            <?= $this->Form->create($table) ?>
            <fieldset>
                <legend><?= __('Add Table') ?></legend>
                <?php
                    echo $this->Form->control('name');
                    echo $this->Form->control('alias');
                    echo $this->Form->control('pos');
                    echo $this->Form->control('table_group_id', ['options' => $tableGroups]);
                    echo $this->Form->control('include');
                    echo $this->Form->control('type');
                    echo $this->Form->control('is_visible');
                    echo $this->Form->control('options');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
