<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Table[]|\Cake\Collection\CollectionInterface $tables
 */
?>
<div class="tables index content">
    <?= $this->Html->link(__('New Table'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Tables') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('name') ?></th>
                    <th><?= $this->Paginator->sort('alias') ?></th>
                    <th><?= $this->Paginator->sort('pos') ?></th>
                    <th><?= $this->Paginator->sort('table_group_id') ?></th>
                    <th><?= $this->Paginator->sort('include') ?></th>
                    <th><?= $this->Paginator->sort('type') ?></th>
                    <th><?= $this->Paginator->sort('is_visible') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($tables as $table): ?>
                <tr>
                    <td><?= $this->Number->format($table->id) ?></td>
                    <td><?= h($table->name) ?></td>
                    <td><?= h($table->alias) ?></td>
                    <td><?= $this->Number->format($table->pos) ?></td>
                    <td><?= $table->has('table_group') ? $this->Html->link($table->table_group->name, ['controller' => 'TableGroups', 'action' => 'view', $table->table_group->id]) : '' ?></td>
                    <td><?= h($table->include) ?></td>
                    <td><?= h($table->type) ?></td>
                    <td><?= h($table->is_visible) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('View'), ['action' => 'view', $table->id]) ?>
                        <?= $this->Html->link(__('Edit'), ['action' => 'edit', $table->id]) ?>
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $table->id], ['confirm' => __('Are you sure you want to delete # {0}?', $table->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
