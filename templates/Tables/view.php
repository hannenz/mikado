<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Table $table
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('Edit Table'), ['action' => 'edit', $table->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Table'), ['action' => 'delete', $table->id], ['confirm' => __('Are you sure you want to delete # {0}?', $table->id), 'class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('List Tables'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
            <?= $this->Html->link(__('New Table'), ['action' => 'add'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="tables view content">
            <h3><?= h($table->name) ?></h3>
            <table>
                <tr>
                    <th><?= __('Name') ?></th>
                    <td><?= h($table->name) ?></td>
                </tr>
                <tr>
                    <th><?= __('Alias') ?></th>
                    <td><?= h($table->alias) ?></td>
                </tr>
                <tr>
                    <th><?= __('Table Group') ?></th>
                    <td><?= $table->has('table_group') ? $this->Html->link($table->table_group->name, ['controller' => 'TableGroups', 'action' => 'view', $table->table_group->id]) : '' ?></td>
                </tr>
                <tr>
                    <th><?= __('Include') ?></th>
                    <td><?= h($table->include) ?></td>
                </tr>
                <tr>
                    <th><?= __('Type') ?></th>
                    <td><?= h($table->type) ?></td>
                </tr>
                <tr>
                    <th><?= __('Id') ?></th>
                    <td><?= $this->Number->format($table->id) ?></td>
                </tr>
                <tr>
                    <th><?= __('Pos') ?></th>
                    <td><?= $this->Number->format($table->pos) ?></td>
                </tr>
                <tr>
                    <th><?= __('Is Visible') ?></th>
                    <td><?= $table->is_visible ? __('Yes') : __('No'); ?></td>
                </tr>
            </table>
            <div class="text">
                <strong><?= __('Options') ?></strong>
                <blockquote>
                    <?= $this->Text->autoParagraph(h($table->options)); ?>
                </blockquote>
            </div>
            <div class="related">
                <h4><?= __('Related Fields') ?></h4>
                <?php if (!empty($table->fields)) : ?>
                <div class="table-responsive">
                    <table>
                        <tr>
                            <th><?= __('Id') ?></th>
                            <th><?= __('Name') ?></th>
                            <th><?= __('Alias') ?></th>
                            <th><?= __('Type') ?></th>
                            <th><?= __('Options') ?></th>
                            <th><?= __('Default') ?></th>
                            <th><?= __('Table Id') ?></th>
                            <th class="actions"><?= __('Actions') ?></th>
                        </tr>
                        <?php foreach ($table->fields as $fields) : ?>
                        <tr>
                            <td><?= h($fields->id) ?></td>
                            <td><?= h($fields->name) ?></td>
                            <td><?= h($fields->alias) ?></td>
                            <td><?= h($fields->type) ?></td>
                            <td><?= h($fields->options) ?></td>
                            <td><?= h($fields->default) ?></td>
                            <td><?= h($fields->table_id) ?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('View'), ['controller' => 'Fields', 'action' => 'view', $fields->id]) ?>
                                <?= $this->Html->link(__('Edit'), ['controller' => 'Fields', 'action' => 'edit', $fields->id]) ?>
                                <?= $this->Form->postLink(__('Delete'), ['controller' => 'Fields', 'action' => 'delete', $fields->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fields->id)]) ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
