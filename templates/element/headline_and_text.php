<div class="headline-and-text">
    <h2 class="headline"><?= $this->getContent('head1') ?></h2>
    <div class="body-text"><?= $this->getContent('text1') ?></div>
</div>
<?php
/*
 * CMT Terminology:
 * ContentGroup             -> slot
 * hasMany Object           -> element ??
 * hasMany Element          -> atom ??
 */
