<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * LocalesFixture
 */
class LocalesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'lc' => 'Lore',
                'pos' => 1,
                'created' => '2022-07-31 19:42:16',
                'modified' => '2022-07-31 19:42:16',
            ],
        ];
        parent::init();
    }
}
