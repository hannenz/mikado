<?php
declare(strict_types=1);

namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PageTemplatesFixture
 */
class PageTemplatesFixture extends TestFixture
{
    /**
     * Init method
     *
     * @return void
     */
    public function init(): void
    {
        $this->records = [
            [
                'id' => 1,
                'name' => 'Lorem ipsum dolor sit amet',
                'file' => 'Lorem ipsum dolor sit amet',
                'pos' => 1,
                'created' => '2022-07-18 19:59:18',
                'modified' => '2022-07-18 19:59:18',
            ],
        ];
        parent::init();
    }
}
