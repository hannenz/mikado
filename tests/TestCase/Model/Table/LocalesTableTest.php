<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\LocalesTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\LocalesTable Test Case
 */
class LocalesTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\LocalesTable
     */
    protected $Locales;

    /**
     * Fixtures
     *
     * @var array<string>
     */
    protected $fixtures = [
        'app.Locales',
    ];

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Locales') ? [] : ['className' => LocalesTable::class];
        $this->Locales = $this->getTableLocator()->get('Locales', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->Locales);

        parent::tearDown();
    }

    /**
     * Test validationDefault method
     *
     * @return void
     * @uses \App\Model\Table\LocalesTable::validationDefault()
     */
    public function testValidationDefault(): void
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
