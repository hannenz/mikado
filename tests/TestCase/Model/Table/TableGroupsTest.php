<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TableGroups;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TableGroups Test Case
 */
class TableGroupsTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TableGroups
     */
    protected $TableGroups;

    /**
     * setUp method
     *
     * @return void
     */
    protected function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Groups') ? [] : ['className' => TableGroups::class];
        $this->TableGroups = $this->getTableLocator()->get('Groups', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    protected function tearDown(): void
    {
        unset($this->TableGroups);

        parent::tearDown();
    }
}
