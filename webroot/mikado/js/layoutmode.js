class Layoutmode {

    constructor() {
        const div = document.createElement('div');
        div.innerHTML = 'This is the so-called layoutmode';
        div.style.cssText = 'padding: 1rem; color: #fff; background-color: tomato; text-align: center;';
        document.body.prepend(div);

        const elements = document.querySelectorAll('[data-element-id]');
        elements.forEach((el) => {
            el.style.outline = '3px dotted magenta';
            el.style.outlineOffset = '2px';
            el.setAttribute('contenteditable', true);
            el.addEventListener('input', (e) => {
                console.log('Element has been altered', el.innerText);
            });
        });
    }
}

document.addEventListener('DOMContentLoaded', () => {
    new Layoutmode();
});
